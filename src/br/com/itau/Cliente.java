package br.com.itau;

public class Cliente {
    private String nome = "";
    private float valorParaInvestir = 0f;
    private int  mesesCapitalInvestido = 0;

    public Cliente(String nome, float valorParaInvestir, int mesesCapitalInvestido){
        this.nome = nome;
        this.valorParaInvestir = valorParaInvestir;
        this.mesesCapitalInvestido = mesesCapitalInvestido;
    }

    public String getNome(){
        return this.nome;
    }

    public float getValorParaInvestir(){
        return this.valorParaInvestir;
    }

    public int getMesesCapitalInvestido(){
        return this.mesesCapitalInvestido;
    }
}
