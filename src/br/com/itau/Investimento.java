package br.com.itau;

public class Investimento {
    private int mesesCapitalInvestido = 0;
    private float valorSimulacao = 0f;
    private float valorCapitalInvestido = 0f;
    private double fatorInvestimentoProduto = 0;


    public Investimento(Cliente cliente, Produto produto){
        this.fatorInvestimentoProduto = 1 + produto.getTaxaInvestimento() / 100;
        this.valorCapitalInvestido = cliente.getValorParaInvestir();
        this.mesesCapitalInvestido = cliente.getMesesCapitalInvestido();
    }

    public float getSimulacaoInvestimento(){
        valorSimulacao = valorCapitalInvestido;

        for(int i = 1; i <= mesesCapitalInvestido; i++){
            valorSimulacao = (float) (fatorInvestimentoProduto * valorSimulacao);
        }

       return valorSimulacao;
    }
}
