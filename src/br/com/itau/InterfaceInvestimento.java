package br.com.itau;

import java.util.Scanner;

public class InterfaceInvestimento {
    Scanner scanner = new Scanner(System.in);
    private String nome = "";
    private float valorParaInvestir = 0f;
    private int  mesesCapitalInvestido = 0;

    public InterfaceInvestimento(){}

    public void setNome(){
        System.out.print("Olá, qual o seu nome? ");
        this.nome = scanner.next();
    }

    public void setValorParaInvestir(){
        System.out.print("Qual o valor que deseja investir? ");
        this.valorParaInvestir = scanner.nextFloat();
    }

    public void setMesesCapitalInvestido(){
        System.out.print("Por quantos meses deseja deixar o capital investido? ");
        this.mesesCapitalInvestido = scanner.nextInt();
    }

    public String getNome(){
        return this.nome;
    }

    public float getValorParaInvestir(){
        return this.valorParaInvestir;
    }

    public int getMesesCapitalInvestido(){
        return this.mesesCapitalInvestido;
    }

    public void getMensagemRetornoInvestimento(Investimento investimento, Cliente cliente){
        System.out.print(cliente.getNome() + " ao final do período, você terá um total de " + investimento.getSimulacaoInvestimento());
    }
}
