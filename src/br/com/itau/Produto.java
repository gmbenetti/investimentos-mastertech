package br.com.itau;

public class Produto {
    private String nome = "Investimento 01";
    private int id = 1;
    private double taxaInvestimento = 0.7f;

    public Produto(){}

    public String getNome() {
        return this.nome;
    }

    public int getId() {
        return this.id;
    }

    public double getTaxaInvestimento() {
        return this.taxaInvestimento;
    }
}
