package br.com.itau;

public class Main {

    public static void main(String[] args) {

        InterfaceInvestimento interfaceInvestimento = new InterfaceInvestimento();

        interfaceInvestimento.setNome();
        interfaceInvestimento.setValorParaInvestir();
        interfaceInvestimento.setMesesCapitalInvestido();

        Cliente cliente = new Cliente(
                interfaceInvestimento.getNome(),
                interfaceInvestimento.getValorParaInvestir(),
                interfaceInvestimento.getMesesCapitalInvestido());

        Produto produtoInvestido = new Produto();

        Investimento investimento = new Investimento(cliente, produtoInvestido);

        interfaceInvestimento.getMensagemRetornoInvestimento(investimento, cliente);
    }
}
